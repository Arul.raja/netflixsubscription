package fakes;

import service.BillingService;

public class MockBillingService extends BillingService {
    public int noOfBillDetailsInvocations=0;
    @Override
    public void billDetails(double amount) {
        noOfBillDetailsInvocations++;
    }
}
