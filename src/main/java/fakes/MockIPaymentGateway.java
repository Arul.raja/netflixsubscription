package fakes;

import payment.IPaymentGateway;
import payment.TransactionStatus;

public class MockIPaymentGateway implements IPaymentGateway {
    public TransactionStatus transactionStatus;

    public MockIPaymentGateway(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @Override
    public TransactionStatus pay(double amount) {
        return transactionStatus;
    }
}
