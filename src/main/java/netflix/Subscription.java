package netflix;

import payment.IPaymentGateway;
import payment.TransactionStatus;
import service.BillingService;

public class Subscription {
    int id;
    SubscriptionPlan subscriptionPlan;
    BillingService billingService;
    IPaymentGateway paymentGateway;
    SubscriptionStatus subscriptionStatus = SubscriptionStatus.INACTIVE;

    public Subscription(int id, SubscriptionPlan subscriptionPlan,BillingService billingService,IPaymentGateway paymentGateway) {
        this.id = id;
        this.subscriptionPlan = subscriptionPlan;
        this.billingService = billingService;
        this.paymentGateway = paymentGateway;
    }

    public void activate() {
        billingService.billDetails(subscriptionPlan.getPrice());
        TransactionStatus transactionStatus = paymentGateway.pay(subscriptionPlan.getPrice());
        if(transactionStatus == TransactionStatus.SUCCESS){
            subscriptionStatus = SubscriptionStatus.ACTIVE;
        }
    }
    public boolean  isSubscriptionActive(){
        return subscriptionStatus==SubscriptionStatus.ACTIVE;
    }
}
