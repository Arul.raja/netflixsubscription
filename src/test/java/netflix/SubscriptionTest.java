package netflix;

import fakes.MockBillingService;
import fakes.MockIPaymentGateway;
import org.junit.jupiter.api.Test;
import payment.IPaymentGateway;
import payment.TransactionStatus;
import service.BillingService;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class SubscriptionTest {
    @Test
    void dummyTest() {
        assertThat(1, is(equalTo(1)));
    }

    @Test
    void shouldDisplayPlanDetailsAndAmountBeforePaymentForTheNetflixSubscriber() {
        int id = 12345;
        BillingService mockBillingService = mock(BillingService.class);
        IPaymentGateway mockIPaymentGateway = mock(IPaymentGateway.class);
        Subscription subscription = new Subscription(id, SubscriptionPlan.YEARLY, mockBillingService, mockIPaymentGateway);

        subscription.activate();

        verify(mockBillingService).billDetails(SubscriptionPlan.YEARLY.getPrice());

    }

    @Test
    void shouldActivateSubscriptionWhenThePaymentTransactionIsSuccessful() {
        int id = 12345;
        double amount = SubscriptionPlan.YEARLY.getPrice();
        BillingService mockBillingService = mock(BillingService.class);
        IPaymentGateway mockIPaymentGateway = mock(IPaymentGateway.class);
        when(mockIPaymentGateway.pay(amount)).thenReturn(TransactionStatus.SUCCESS);
        Subscription subscription = new Subscription(id, SubscriptionPlan.YEARLY, mockBillingService, mockIPaymentGateway);

        subscription.activate();

        verify(mockIPaymentGateway).pay(amount);
    }
    @Test
    void shouldNotActivateSubscriptionWhenThePaymentTransactionIsFailure(){
        int id =12345;
        double amount = SubscriptionPlan.YEARLY.getPrice();
        BillingService mockBillingService = mock(BillingService.class);
        IPaymentGateway mockIPaymentGateway = mock(IPaymentGateway.class);
        when(mockIPaymentGateway.pay(amount)).thenReturn(TransactionStatus.FAILURE);
        Subscription subscription = new Subscription(id,SubscriptionPlan.YEARLY,mockBillingService,mockIPaymentGateway);

        subscription.activate();

        verify(mockIPaymentGateway).pay(amount);
    }
    @Test
    void shouldReturnTrueWhenTheSubscriptionStatusIsActive(){
        int id =12345;
        double amount = SubscriptionPlan.YEARLY.getPrice();
        BillingService mockBillingService = mock(BillingService.class);
        IPaymentGateway mockIPaymentGateway = mock(IPaymentGateway.class);
        when(mockIPaymentGateway.pay(amount)).thenReturn(TransactionStatus.SUCCESS);
        Subscription subscription = new Subscription(id,SubscriptionPlan.YEARLY,mockBillingService,mockIPaymentGateway);

        subscription.activate();
        boolean actual = subscription.isSubscriptionActive();

        assertTrue(actual);
    }

    @Test
    void shouldReturnFalseWhenTheSubscriptionStatusIsInActive(){
        int id =12345;
        double amount = SubscriptionPlan.YEARLY.getPrice();
        BillingService mockBillingService = mock(BillingService.class);
        IPaymentGateway mockIPaymentGateway = mock(IPaymentGateway.class);
        when(mockIPaymentGateway.pay(amount)).thenReturn(TransactionStatus.FAILURE);
        Subscription subscription = new Subscription(id,SubscriptionPlan.YEARLY,mockBillingService,mockIPaymentGateway);
        subscription.activate();
        boolean actual = subscription.isSubscriptionActive();

        assertFalse(actual);

    }
    @Test
    void shouldDisplayPlanDetailsAndAmountBeforePaymentForTheNetflixSubscriberUsingFakes() {
        int id = 12345;
        MockBillingService mockBillingService = new MockBillingService();
        MockIPaymentGateway mockIPaymentGateway = new MockIPaymentGateway(TransactionStatus.SUCCESS);
        Subscription subscription = new Subscription(id, SubscriptionPlan.YEARLY, mockBillingService, mockIPaymentGateway);

        subscription.activate();

        assertEquals(1,mockBillingService.noOfBillDetailsInvocations);
    }
    @Test
    void shouldActivateSubscriptionWhenThePaymentTransactionIsSuccessfulUsingFakes() {
        int id = 12345;
        MockBillingService mockBillingService = new MockBillingService();
        MockIPaymentGateway mockIPaymentGateway = new MockIPaymentGateway(TransactionStatus.SUCCESS);
        Subscription subscription = new Subscription(id, SubscriptionPlan.YEARLY, mockBillingService, mockIPaymentGateway);

        subscription.activate();
        boolean actual = subscription.isSubscriptionActive();

        assertTrue(actual);
    }
    @Test
    void shouldNotActivateSubscriptionWhenThePaymentTransactionIsFailureUsingFakes() {
        int id = 12345;
        MockBillingService mockBillingService = new MockBillingService();
        MockIPaymentGateway mockIPaymentGateway = new MockIPaymentGateway(TransactionStatus.FAILURE);
        Subscription subscription = new Subscription(id, SubscriptionPlan.YEARLY, mockBillingService, mockIPaymentGateway);

        subscription.activate();
        boolean actual = subscription.isSubscriptionActive();

        assertFalse(actual);
    }
}

